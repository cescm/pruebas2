select * from prueba

truncate table prueba
truncate table pentaho_logging.step_logging
truncate table pentaho_logging.metrics_log
truncate table pentaho_logging.job_logs
truncate table pentaho_logging.job_logs_table

create schema pentaho_logging
select * from sys.schemas where principal_id=1
select * from sys.objects where type not in ('S','IT','PK','UQ','SQ')


select * from pentaho_logging.step_logging where id_batch=(select max(id_batch) from pentaho_logging.step_logging)
select * from pentaho_logging.metrics_log where id_batch=(select max(id_batch) from pentaho_logging.metrics_log)

select * from pentaho_logging.job_logs
select * from pentaho_logging.job_logs_table

create view VISTA_TIEMPO as
select id_batch,convert(varchar(12),(max(metrics_date) - min(metrics_date)),114) as tiempo_consumido 
from pentaho_logging.metrics_log 
group by id_batch

alter view VISTA_LOGS AS
SELECT st.id_batch,st.log_date,st.transname,st.stepname,st.errors,st.log_field,vt.tiempo_consumido
FROM pentaho_logging.step_logging st left join vista_tiempo vt on st.id_batch=vt.id_batch

select * from pentaho_logging.step_logging where log_field like '%error%'

select * from vista_tiempo

select * from pentaho_logging.metrics_log